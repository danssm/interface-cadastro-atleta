package Model;

public class Atleta {
	protected String nome;
	protected String idade;
	protected String peso;
	protected Endereco endereco;
 	
 	
	public Atleta(String nome) {
 	
		this.nome = nome;
	}
	public String getNome() {
 			return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
 	}
 	public String getIdade() {
 		return idade;
 	}
 	public void setIdade(String idade) {
 		this.idade = idade;
 	}
 	public String getPeso() {
 		return peso;
 	}
 	public void setPeso(String peso) {
 		this.peso = peso;
 	}
 	public Endereco getEndereco() {
 		return endereco;
 	}
 	public void setEndereco(Endereco endereco) {
 		this.endereco = endereco;
 	}
}