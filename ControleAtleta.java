package Controller;
import java.util.ArrayList;
import Model.JogVoley;
import Model.JogKarate;


public class ControleAtleta {
	
	private ArrayList<JogVoley> listaJogVoley;
	private ArrayList<JogKarate> listaJogKarate;

	public ControleAtleta() {
		listaJogVoley = new ArrayList<JogVoley>();
		listaJogKarate = new ArrayList<JogKarate>();
	}

	public void adicionar(JogVoley umJogVoley) {
		
		listaJogVoley.add(umJogVoley);
	}
	
	
	public void adicionar(JogKarate umJogKarate) {
		listaJogKarate.add(umJogKarate);
	}
	
	public void remover(JogVoley umJogVoley) {
		listaJogVoley.remove(umJogVoley);
	}
	
	public JogVoley buscarJogVoley(String umNome) {
		for (JogVoley umJogVoley : listaJogVoley) {
			if (umJogVoley.getNome().equalsIgnoreCase(umNome)){
				return umJogVoley;
			}
		}
		return null;
	}
	
	public JogKarate buscarJogKarate(String nome) {
		for (JogKarate umJogKarate : listaJogKarate) {
			if (umJogKarate.getNome().equalsIgnoreCase(nome)){
				return umJogKarate;
			}
		}
		return null;
	}

	public void listarJogVoley() {
		for(JogVoley umJogVoley : listaJogVoley) {
			System.out.println(umJogVoley.getNome());
		}
	} 
	
	public void listarJogKarate() {
		for(JogKarate umJogKarate : listaJogKarate) {
			System.out.println(umJogKarate.getNome());
		}
	}
}
