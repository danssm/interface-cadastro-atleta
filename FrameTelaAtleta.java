package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import Model.JogVoley;
import Model.JogKarate;
import Model.Endereco;
import Model.Atleta;
import Controller.ControleAtleta;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JLabel;

public class FrameTelaAtleta extends JFrame {

	private JPanel contentPane;
	private JTextField textNome;
	private final JRadioButton opcaoJogKarate = new JRadioButton("Lutador de Karatê");
	private JTextField textIdade;
	private JTextField textPeso;
	private JTextField textCidade;
	private JTextField textEstado;
	private JTextField textPais;
	private JTextField textCep;
	
	Scanner ler = new Scanner(System.in);
	JogKarate umJogKarate;
	JogVoley umJogVoley;
	Endereco umEndereco;
	ControleAtleta umControlAtleta = new ControleAtleta ();
	private int num;
	private JTextField textAltura;
	private JTextField textAgilidade;
	private JTextField textGraduacao;
	private JTextField textGolpeForte;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameTelaAtleta frame = new FrameTelaAtleta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameTelaAtleta() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 578, 397);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(238, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textNome = new JTextField();
		
		textNome.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textNome.setBackground(Color.WHITE);
		textNome.setForeground(Color.BLACK);
		textNome.setHorizontalAlignment(SwingConstants.CENTER);
		textNome.setBounds(390, 42, 162, 32);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		
		
		JRadioButton opcaoJogVoley = new JRadioButton("Jogador de Voley");
		opcaoJogVoley.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num = 1;
				
			}
		});
		opcaoJogVoley.setBounds(6, 54, 143, 23);
		contentPane.add(opcaoJogVoley);
		opcaoJogKarate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num = 2;
			}
		});
		opcaoJogKarate.setBounds(157, 51, 143, 29);
		contentPane.add(opcaoJogKarate);
		
		JLabel lblNewLabel = new JLabel("Cadastro de Atletas");
		lblNewLabel.setBackground(Color.LIGHT_GRAY);
		lblNewLabel.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 17));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(182, 15, 189, 23);
		contentPane.add(lblNewLabel);
		
		JButton botaoCadastrar = new JButton("Cadastrar");
		botaoCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(num==1){
					umJogVoley = new JogVoley(textNome, textAltura);
					
				}
			}
		});
		botaoCadastrar.setBounds(6, 107, 143, 29);
		contentPane.add(botaoCadastrar);
		
		JButton botaoRemover = new JButton("Remover");
		botaoRemover.setBounds(157, 107, 143, 29);
		contentPane.add(botaoRemover);
		
		JButton botaoFicha = new JButton("Listar ficha");
		botaoFicha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		botaoFicha.setBounds(86, 148, 143, 29);
		contentPane.add(botaoFicha);
		
		JLabel lblNome = new JLabel("Nome :");
		lblNome.setBounds(334, 50, 61, 16);
		contentPane.add(lblNome);
		
		textIdade = new JTextField();
		textIdade.setHorizontalAlignment(SwingConstants.CENTER);
		textIdade.setForeground(Color.BLACK);
		textIdade.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textIdade.setColumns(10);
		textIdade.setBackground(Color.WHITE);
		textIdade.setBounds(390, 90, 162, 32);
		contentPane.add(textIdade);
		
		textPeso = new JTextField();
		textPeso.setHorizontalAlignment(SwingConstants.CENTER);
		textPeso.setForeground(Color.BLACK);
		textPeso.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textPeso.setColumns(10);
		textPeso.setBackground(Color.WHITE);
		textPeso.setBounds(390, 138, 162, 32);
		contentPane.add(textPeso);
		
		textCidade = new JTextField();
		textCidade.setHorizontalAlignment(SwingConstants.CENTER);
		textCidade.setForeground(Color.BLACK);
		textCidade.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textCidade.setColumns(10);
		textCidade.setBackground(Color.WHITE);
		textCidade.setBounds(390, 186, 162, 32);
		contentPane.add(textCidade);
		
		textEstado = new JTextField();
		textEstado.setHorizontalAlignment(SwingConstants.CENTER);
		textEstado.setForeground(Color.BLACK);
		textEstado.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textEstado.setColumns(10);
		textEstado.setBackground(Color.WHITE);
		textEstado.setBounds(390, 234, 162, 32);
		contentPane.add(textEstado);
		
		JLabel lblIdade = new JLabel("Idade :");
		lblIdade.setBounds(334, 98, 61, 16);
		contentPane.add(lblIdade);
		
		JLabel lblPeso = new JLabel("Peso :");
		lblPeso.setBounds(334, 146, 61, 16);
		contentPane.add(lblPeso);
		
		JLabel lblCidade = new JLabel("Cidade :");
		lblCidade.setBounds(334, 194, 61, 16);
		contentPane.add(lblCidade);
		
		JLabel lblEstado = new JLabel("Estado :");
		lblEstado.setBounds(334, 242, 61, 16);
		contentPane.add(lblEstado);
		
		textPais = new JTextField();
		textPais.setHorizontalAlignment(SwingConstants.CENTER);
		textPais.setForeground(Color.BLACK);
		textPais.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textPais.setColumns(10);
		textPais.setBackground(Color.WHITE);
		textPais.setBounds(390, 282, 162, 32);
		contentPane.add(textPais);
		
		textCep = new JTextField();
		textCep.setHorizontalAlignment(SwingConstants.CENTER);
		textCep.setForeground(Color.BLACK);
		textCep.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textCep.setColumns(10);
		textCep.setBackground(Color.WHITE);
		textCep.setBounds(390, 330, 162, 32);
		contentPane.add(textCep);
		
		JLabel lblPas = new JLabel("País :");
		lblPas.setBounds(334, 290, 61, 16);
		contentPane.add(lblPas);
		
		JLabel lblCep = new JLabel("CEP :");
		lblCep.setBounds(334, 338, 61, 16);
		contentPane.add(lblCep);
		
		textAltura = new JTextField();
		textAltura.setHorizontalAlignment(SwingConstants.CENTER);
		textAltura.setForeground(Color.BLACK);
		textAltura.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textAltura.setColumns(10);
		textAltura.setBackground(Color.WHITE);
		textAltura.setBounds(131, 189, 162, 32);
		contentPane.add(textAltura);
		
		textAgilidade = new JTextField();
		textAgilidade.setHorizontalAlignment(SwingConstants.CENTER);
		textAgilidade.setForeground(Color.BLACK);
		textAgilidade.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textAgilidade.setColumns(10);
		textAgilidade.setBackground(Color.WHITE);
		textAgilidade.setBounds(131, 237, 162, 32);
		contentPane.add(textAgilidade);
		
		textGraduacao = new JTextField();
		textGraduacao.setHorizontalAlignment(SwingConstants.CENTER);
		textGraduacao.setForeground(Color.BLACK);
		textGraduacao.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textGraduacao.setColumns(10);
		textGraduacao.setBackground(Color.WHITE);
		textGraduacao.setBounds(131, 285, 162, 32);
		contentPane.add(textGraduacao);
		
		textGolpeForte = new JTextField();
		textGolpeForte.setHorizontalAlignment(SwingConstants.CENTER);
		textGolpeForte.setForeground(Color.BLACK);
		textGolpeForte.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		textGolpeForte.setColumns(10);
		textGolpeForte.setBackground(Color.WHITE);
		textGolpeForte.setBounds(131, 333, 162, 32);
		contentPane.add(textGolpeForte);
		
		JLabel lblAltura = new JLabel("Altura :");
		lblAltura.setBounds(74, 197, 61, 16);
		contentPane.add(lblAltura);
		
		JLabel lblAgilidade = new JLabel("Agilidade (0 a 10) :");
		lblAgilidade.setBounds(6, 245, 129, 16);
		contentPane.add(lblAgilidade);
		
		JLabel lblGraduao = new JLabel("Graduação :");
		lblGraduao.setBounds(50, 293, 85, 16);
		contentPane.add(lblGraduao);
		
		JLabel lblGolpeForte = new JLabel("Golpe Forte :");
		lblGolpeForte.setBounds(44, 341, 91, 16);
		contentPane.add(lblGolpeForte);
	}
}
